import React, { useState } from "react";
import "./App.css";
import Accordions from "./components/Accordions";
import IAccordion from "./models/IAccordion";
import { AccordionData } from "./data/AccordionData";

function App() {
  const [count, setCount] = useState(0);
  const [mydata, setData] = useState<IAccordion[]>(AccordionData);

  const onToggleVisibility = (id: number) => {
    console.log(id);

    const updatedFixtures = [...mydata];
    const fixture = updatedFixtures[id];
    updatedFixtures[id] = {
      ...fixture,
      isVisible: !fixture.isVisible
    };
    setData(updatedFixtures)
    console.log(updatedFixtures);
  };
  return (
    <div className="App">
      <Accordions
        accordionProps={mydata}
        onToggleVisibility={onToggleVisibility}
      />
    </div>
  );
}

export default App;
