export default interface IAccordion {
  id: number;
  question: string;
  answer: string;
  isVisible:boolean;
}
