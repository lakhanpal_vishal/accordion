import React from "react";
import IAccordion from "../models/IAccordion";
import Accordion from "./Accordion";

interface IAccordionProps {
  accordionProps: IAccordion[];
  onToggleVisibility: (id: number) => void;
}

const Accordions: React.FC<IAccordionProps> = (props) => {
  return (
    <div className="flex flex-grow min-h-screen  bg-purple-900 justify-center items-center ">
      <div className=" pb-4 w-8/12 mt-12 mb-10 bg-gray-100  dark:bg-gray-800 rounded-lg shadow">
        <div className="mt-4 grid grid-cols-3 -space-x-6 mr-8">
          <h1 className="ml-8 w-3/4 mt-6 h-12 text-2xl  text-left font-semibold tracking-widest opacity-70 capitalize">
            questions and answers about login
          </h1>        
          {props.accordionProps.map((data, index) => (
            <Accordion
              key={index}
              item={data}
              onToggleVisibility={props.onToggleVisibility}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default Accordions;
