import React, { FC } from "react";

interface TDP {
  item: { id: number; question: string; answer: string; isVisible: boolean };
  onToggleVisibility: (id: number) => void;
}

const Accordion: FC<TDP> = (props) => {
  return (
    <div className=" border-2 mr-12 mt-3 col-start-2 col-end-4 ">
      <div className="ml-2 w-11/12  h-12">
        <div className="flex mb-4 ">
          <div className="w-5/6 h-10  text-left tracking-widest opacity-70 font-bold  ml-4 mt-3">
            {props.item.question}
          </div>

          <div className="ml-auto -mr-4 mt-2">
            <button
              className="bg-gray-200 rounded-full border border-none h-8 w-8 flex text-lg items-center justify-center
           text-red-900 opacity-60 focus:outline-none"
              onClick={() => props.onToggleVisibility(props.item.id)}
            >
              {props.item.isVisible ? "-" : "+"}
            </button>
          </div>
        </div>
      </div>
      {props.item.isVisible && (
        <div className="ml-6 text-justify mr-10 opacity-60 mb-2">
          {props.item.answer}
        </div>
      )}
    </div>
  );
};
export default Accordion;
